package com.example.demo;

import com.example.demo.entity.Label;
import com.example.demo.entity.LogEntity;
import com.example.demo.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import static com.example.demo.enums.LogLevel.ERROR;
import static com.example.demo.enums.LogLevel.FATAL;
import static com.example.demo.enums.LogLevel.INFO;
import static com.example.demo.enums.LogLevel.TRACE;

@SpringBootApplication
public class DemoApplication {

    @Autowired
    private LogRepository logRepository;


    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }


    /*
        Init of some data
     */

    @PostConstruct
    public void init() {

        Label label1 = Label.builder()
                .key("app")
                .value("foo-app")
                .build();
        Label label2 = Label.builder()
                .key("process_id")
                .value("1238")
                .build();
        Label label3 = Label.builder()
                .key("host")
                .value("10.128.12.1")
                .build();

        List<Label> labels = new ArrayList<>();
        labels.add(label1);
        labels.add(label2);
        labels.add(label3);


        logRepository.save(LogEntity.builder()
                .level(FATAL)
                .message("M1")
                .timestamps("34254323")
                .labels(labels)
                .build());

        logRepository.save(LogEntity.builder()
                .level(ERROR)
                .message("M2")
                .timestamps("34254323")
                .build());

        logRepository.save(LogEntity.builder()
                .level(INFO)
                .message("M3")
                .timestamps("73672323")
                .build());

        logRepository.save(LogEntity.builder()
                .level(TRACE)
                .message("M4")
                .timestamps("73672323")
                .build());


    }
}
