package com.example.demo.repository;

import com.example.demo.entity.LogEntity;
import com.example.demo.enums.LogLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface LogRepository extends JpaRepository<LogEntity,Long> {

    List<LogEntity> getAllByLevelLessThanEqualAndLabelsKeyInAndLabelsValueIn(LogLevel level, List<String> labelsKey, List<String> labelsValue);

    List<LogEntity> getAllByLevelLessThanEqual(LogLevel level);

}
