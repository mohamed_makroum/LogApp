package com.example.demo.controller;

import com.example.demo.entity.Label;
import com.example.demo.entity.LogEntity;
import com.example.demo.enums.LogLevel;
import com.example.demo.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/test")
public class testController {

    @Autowired
    private LogRepository logRepository;

    @PostMapping
    public void storeLogs(@RequestBody List<LogEntity> logEntitys) {
        logRepository.saveAll(logEntitys);
    }

    /*
    @param labels made it not required so I can fetch by max log level only
     */

    @GetMapping(name = "/fetchBy")
    public List<LogEntity> fetch(@RequestParam String maxLevel, @RequestBody(required = false) List<Label> labels){

        /*
        condition that make fetch only with max log level when labels is null or empty
         */
        if (labels==null || labels.isEmpty()){
            return logRepository.getAllByLevelLessThanEqual(LogLevel.valueOf(maxLevel));
        }

        /*
        extract label attribut from the list
         */

        List<String> keys = labels.stream().map(Label::getKey).collect(Collectors.toList());
        List<String> value = labels.stream().map(Label::getValue).collect(Collectors.toList());

        return logRepository.getAllByLevelLessThanEqualAndLabelsKeyInAndLabelsValueIn(
                LogLevel.valueOf(maxLevel),keys,value);

    }

    @GetMapping(value ="/all")
    public List<LogEntity> fetchAll(){
        return logRepository.findAll();

    }
}
