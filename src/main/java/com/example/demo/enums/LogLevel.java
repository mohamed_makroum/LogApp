package com.example.demo.enums;

public enum LogLevel {

    FATAL, ERROR,INFO,TRACE
}
